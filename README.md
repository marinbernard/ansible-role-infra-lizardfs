# Ansible role infra/lizardfs

Deploy and configure LizardFS clusters.

## Role variables
### Overview
The role relies on many variables, which are dociumented in this section.
All variables begin with the role prefix ``infra_lizardfs_``. Furthermore,
each variable embeds a contextual infix showing which part of the role it
relates to. Here is the list of those contextual prefixes:

  * ``infra_lizardfs_master_*``  
    These variables deal with the setup and configuration of the LizardFS
    **master** component.

  * ``infra_lizardfs_chunkserver_*``  
    These variables deal with the setup and configuration of the LizardFS
    **chunkserver** component.

  * ``infra_lizardfs_cgi_pages_*``  
    These variables deal with the setup and configuration of the set of
    **CGI pages** dedicated to the web supervision of a LizardFS cluster.

  * ``infra_lizardfs_cgi_server_*``  
    These variables deal with the setup and configuration of the LizardFS
    **CGI server** component.

  * ``infra_lizardfs_metalogger_*``  
    These variables deal with the setup and configuration of the LizardFS
    **metalogger** component.

  * ``infra_lizardfs_client_*``  
    These variables deal with the setup and configuration of the LizardFS
    **client** component.

  * ``infra_lizardfs_admintools_*``  
    These variables deal with the setup and configuration of the LizardFS
    **administration tools**.

### LizardFS master component
#### ``infra_lizardfs_master_present`` _(bool)_  
This variable controls the installation and configuration of the LizardFS
master component. Please note that the master won't be started nor enabled
unless the ``infra_lizardfs_master_enabled`` variable is also set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_enabled`` _(bool)_  
Whether to enable and start the LizardFS master daemon. This variable has no
effect unless the ``infra_lizardfs_master_present`` is also set to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_config_path`` _(string)_  
The full path to the _mfsmaster.cfg_ config file. Paths to other configuration
files are read from the ``infra_lizardfs_master_config_settings`` variable,
or from default mfsmaster.cfg settings if no custom paths were specified.

**Default value**: ``/etc/mfs/mfsmaster.cfg``

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_config_settings`` _(dictionary)_  
A set of options used to build the _mfsmaster.cfg_ config file of the master
instance. This variable is a dictionary, which may include one key per legal _mfsmaster.cfg_ setting name. Key names are validated against a list of known-
good parameter names. Values are also checked to match the expected value
type for a given setting. Setting names are case sensitive. Since all valid
LizardFS settings are written in uppercases, you are required to follow
the same convention.

Below is an example of the use of this variable. It defines two configuration
settings which will be included in the main configuration file of a LizardFS
master instance:

```yml
infra_lizardfs_master_config_settings:
  ADMIN_PASSWORD: "MySecretPassword"
  AUTO_RECOVERY: true
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_lizardfs_master_config_settings`` variable is an empty
dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_exports`` _(list)_  
A list of exports to populate the _mfsexports.cfg_ config file. Each list
item is a dictionary representing a single export. Each dictionary must
comply with the following structure:

```yaml
ip: "<client_ip>"
path: "<exported_path>"
[options:
  <key>: <value>
  ...]
```

The ``options`` dictionary if a set of key-value pairs describing export
options. Each key must match one of the options described in the
mfsexports.cfg(5) man page. Key and option names are case sensitive, and must
be written in lowercase. If the ``options`` dictionary is omitted, the role
will let LizardFS fallback to default export options which are described
in the man page.

Export options are validated by the role. All options are supported, except
the ``ro``, ``rw``, ``readonly``and ``readwrite`` export options: these four
variations are gathered under a single ``readonly`` key which accepts a
boolean value. The example below shows a couple of exports, the first one
being read-only, and the second one read-write:

```yml
infra_lizardfs_master_exports:
  - ip: "172.16.154.34/255.255.255.0"
    path: "/my_ro_path"
    options:
      readonly: true
      password: "mySuperPassword"
      mapall: "nobody:nobody"
  - ip: "192.168.1.1/24"
    path: "/my_rw_path"
    options:
      readonly: false
```
**Default value**  
The default value of this variable is shown below. It renders as a
_mfsexports.cfg_ file which is identical to the one provided with a new
LizardFS installation.
```yml
infra_lizardfs_master_exports:
  - ip: "*"
    path: "/"
    options:
      readonly: false
      alldirs: true
      maproot: "0"
  - ip: "*"
    path: "."
    options:
      readonly: false
```

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_globaliolimits`` _(dictionary)_
This variable allows to define entries of the _globaliolimits.cfg_ config
file. The variable is a dictionary matching the following structure:

```yml
infra_lizardfs_master_globaliolimits:
  <cgroup_subsystem>:
    <cgroup_id>: <limit>
```

Where ``<cgroup_subsystem>`` is the name of a cgroup subsystem (usually
``blkio``), ``<cgroup_id>`` the uid of a cgroup, and ``<limit>`` a bandwidth
limit expressed in bytes per second. Use the special identifier
``unclassified`` to target any cgroup.

**Default value**: ``{}``

New installations of LizardFS do not enforce any limit. This role does the
same: the empty dictionary is rendered as an empty _globaliolimits.cfg_ file.

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_goals`` _(dictionary)_
This variable allows to define custom goals. Each key of the dictionnary
will be rendered as a line of the _mfsgoals.cfg_ config file. The variable
must match the following structure:
```yml
infra_lizardfs_master_goals:
  <numeric_id>:
    [name: <goal_name>]
    [type: <std|xor|ec>]
    [parts:
      data: <data_parts>
      [parity: <parity_parts>]]
    [labels: []]
```
Where:
  * ``<numeric_id>`` is an integer between 1 and 40, inclusive, used
    as a unique identifier for a goal. This the only mandatory key of the
    dictionary.

  * ``<goal_name>`` is a custom name for the goal. The ``name`` key is
    optional: if omitted, the ``<numeric_id>`` will be used as the goal's
    name.

  * The optional ``type`` key, which may take one of the following values,
    defines the type of the goal. If the key is omitted, the goal defaults
    to ``std``.

      * ``std`` for a goal based on standard copies
      * ``xor`` for a goal based on single parity copies
      * ``ec`` for a goal based on erasure coding


  * The ``parts`` key is mandatory when the type of the goal is ``xor`` or
    ``ec``. It is used to specify the number of data and parity chunks for
    the goal:

      * The ``data`` subkey is mandatory for both ``xor`` and ``ec`` goals.
      * The ``parity`` subkey is only mandatory with ``ec``goals.


  * The optional ``labels`` key maps to a list of strings, which represents
    a list of chunkserver labels. If omitted, it defaults to blank with
    ``xor`` and ``ec`` roles, and to as many of the _any label_ character
    (``_``) as needed to match ``<numeric_id>`` with ``std`` goals.

**Default value**  
The default value of this variable is reproduced below. It renders as the
same _mfsgoals.cfg_ content as a new LizardFS installation, explicitly
defining the first five(5) standard goals.
```yml
infra_lizardfs_master_goals:
  1: {}
  2: {}
  3: {}
  4: {}
  5: {}
```

------------------------------------------------------------------------------

#### ``infra_lizardfs_master_topology`` _(list)_
This variable is used to generate the _mfstopology.cfg_ config file. It is
structured as a list of dictionaries matching the pattern below:
```yml
infra_lizardfs_master_topology:
  - ip: <ip>
    rack: <int>
  - ip: <ip>
    rack: <int>
  ...
```

Where ``ip`` maps to a host or network IP address, and ``rack`` to an rack
identifier integer.

Each list entry will be rendered as a line in the _mfstopology.cfg_ config
file. New installations of LizardFS come with a blank topology file. The role
fits this behaviour by providing a blank list as a default value for this
variable.

**Default value**: ``[]``

### LizardFS chunkserver component
#### ``infra_lizardfs_chunkserver_present`` _(bool)_  
This variable controls the installation and configuration of the LizardFS
chunkserver component. Please note that the chunkserver won't be started nor
enabled unless the ``infra_lizardfs_chunkserver_enabled`` variable is also
set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_lizardfs_chunkserver_enabled`` _(bool)_  
Whether to enable and start the LizardFS chunkserver daemon. This variable
has no effect unless the ``infra_lizardfs_chunkserver_present`` is also set
to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_lizardfs_chunkserver_config_path`` _(string)_  
The full path to the _mfschunkserver.cfg_ config file. Paths to other
configuration files are read from the
``infra_lizardfs_chunkserver_config_settings`` variable, or from default
_mfschunkserver.cfg_ settings if no custom paths were specified.

**Default value**: ``/etc/mfs/mfschunkserver.cfg``

------------------------------------------------------------------------------

#### ``infra_lizardfs_chunkserver_config_settings`` _(dictionary)_  
A set of options used to build the _mfschunkserver.cfg_ config file of the
chunkserver instance. This variable is a dictionary, which may include one
key per legal _mfschunkserver.cfg_ setting name. Key names are validated
against a list of known-good parameter names. Values are also validated to
match the expected value type for a given setting. Setting names are case
sensitive. Since all valid LizardFS settings are written in uppercase,
you are required to follow the same convention.

Below is an example of the use of this variable. It defines two configuration
settings which will be included in the main configuration file of a LizardFS
chunkserver instance:

```yml
infra_lizardfs_chunkserver_config_settings:
  HDD_TEST_FREQ: 5
  HDD_PUNCH_HOLES: true
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_lizardfs_chunkserver_config_settings`` variable is an
empty dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

------------------------------------------------------------------------------

#### ``infra_lizardfs_chunkserver_storage`` _(list)_
This variable stores a list of mount points serving as storage for the
LizardFS chunk server. Each list entry will be rendered to a single line in
the _mfshdd.cfg_ config file. This entry is a dictionary comprising only
two keys. The structure if this dictionary is shown below:

```yml
infra_lizardfs_chunkserver_storage:
  - path: <string>
    [retired: <bool>]
```

The ``path`` key maps to the full path to a directory dedicated to LizardFS
storage. The optional ``retired`` key allows to mark a storage point for
removal: if it is set to ``true``, the storage entry will be rendered to
a line with a leading ``*`` in _mfshdd.cfg_.

**Default value**: ``[]``

### LizardFS CGI Web UI
#### ``infra_lizardfs_cgi_pages_present`` _(bool)_  
If this variable is set to ``true``, the role will install the set of CGI
pages implementing the LizardFS supervision Web UI. Note that this variable
only triggers the deployment of the CGI pages: the CGI server may be deployed
by setting the ``infra_lizardfs_cgi_server_present`` variable to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_lizardfs_cgi_server_present`` _(bool)_  
This variable controls the installation and configuration of the LizardFS
CGI server component. Please note that the CGI server won't be started nor
enabled unless the ``infra_lizardfs_cgi_server_enabled`` variable is also
set to ``true``.

Since the CGI server requires the presence of the LizardFS CGI pages, setting
this variable to ``true`` will also deploy the ``lizardfs-cgi`` package.

**Default value**: ``false``

### LizardFS metalogger component
#### ``infra_lizardfs_metalogger_present`` _(bool)_  
This variable controls the installation and configuration of the LizardFS
metalogger component. Please note that the metalogger won't be started nor
enabled unless the ``infra_lizardfs_metalogger_enabled`` variable is also
set to ``true``.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_lizardfs_metalogger_enabled`` _(bool)_  
Whether to enable and start the LizardFS metalogger daemon. This variable
has no effect unless the ``infra_lizardfs_metalogger_present`` is also
set to ``true``.

**Default value**: ``true``

------------------------------------------------------------------------------

#### ``infra_lizardfs_metalogger_config_path`` _(string)_  
The full path to the _mfsmetalogger.cfg_ config file.

**Default value**: ``/etc/mfs/mfsmetalogger.cfg``

------------------------------------------------------------------------------

#### ``infra_lizardfs_metalogger_config_settings`` _(dictionary)_  
A set of options used to build the _mfsmetalogger.cfg_ config file of the
metalogger instance. This variable is a dictionary, which may include one
key per legal _mfsmetalogger.cfg_ setting name. Key names are validated
against a list of known-good parameter names. Values are also checked
to match the expected value type for a given setting. Setting names are case
sensitive. Since all valid LizardFS settings are written in uppercase,
you are required to follow the same convention.

Below is an example of the use of this variable. It defines custom
configuration settings which will be included in the main configuration
file of a LizardFS metalogger instance:

```yml
infra_lizardfs_metalogger_config_settings:
  MASTER_HOST: "mymaster.mydomain"
  BACK_LOGS: 150
```

The role is aware of the default values of all possible settings, and stores
this information in a separate data structure. As a consequence, the default
value of the ``infra_lizardfs_metalogger_config_settings`` variable is an
empty dictionary. Any setting which is not explicitly defined by the user will
be rendered to its default value.

**Default value**: ``{}``

### LizardFS client component
#### ``infra_lizardfs_client_present`` _(bool)_  
This variable controls the installation and configuration of the LizardFS
client component.

**Default value**: ``false``

------------------------------------------------------------------------------

#### ``infra_lizardfs_client_config_path`` _(string)_
This variable set the path to the directory hosting LizardFS configuration
files for the _mfsmount_ client. These files follow the general rules
exposed in the mfsmount.cfg(5) man page. The role will generate one config
file per entry in the ``infra_lizardfs_client_config_mountfiles`` variable.
These files will be stored at the location specified by this variable.

**Default value**: ``/etc/mfs``

------------------------------------------------------------------------------

#### ``infra_lizardfs_client_config_mountfiles`` _(list)_
This variable is a list of LizardFS mount point definitions. Each definition
will be rendered as a single ``mfsmount-<name>.cfg`` config file, where
``<name>`` is the value of the ``config.name`` key from the definition
dictionary.

This variable is structured as follows:

```yml
infra_lizardfs_client_config_mountfiles:
  - path: <string>
    config:
      name: <string>
      [owner: <string>]
      [group: <string>]
      [mode: <string>]
      [create_mountpoint: <bool>]
    [options: {}]
```

Each list item is a dictonary known as a _mount definition_. Only two keys are
mandatory within this dictionary:

  1. ``path``, which maps to the location at which the LizardFS file system
     should be mounted.

  2. ``config.name`` which maps to a string storing the unique name of the
     mount definition. This information will be used to construct the name
     of the configuration file, based on the following pattern:
     ``mfsmount-<name>.cfg``.

The dictionary may also include a bunch of optional keys:

  * ``config.owner`` allows to specify the owner of the configuration file.
    The default value is ``root``.

  * ``config.group`` allows to specify the group of the configuration file.
    The default value is ``root``.

  * ``config.mode`` allows to specify the mode of the configuration file.
    The default value is ``0640``.

  * ``config.create_mountpoint`` instructs the role to create a directory
    at ``path`` if it does not exist. Default is not to create it.

  * ``options`` is a dictionary which allows to pass custom mount options
    to be embedded into the configuration file. The role supports any option
    documented in the mfsmount(8) man page. Each key of the dictionary is
    named after an option name, and maps to the wished value for this option.

**Default value**: ``[]``

### LizardFS administration tools
#### ``infra_lizardfs_admintools_present`` _(bool)_  
This variable controls the installation of LizardFS administration tools.

**Default value**: ``false``

## License
MIT

## Author
Marin Bernard <marin@olivarim.com>
